<div class="box shadowed-box">
<?php	$attributes = array('class' => 'form-signin text-center'); ?>
<?php echo form_open(base_url('member/login/reset_password/' . $code), $attributes);?>
	<img src="<?php echo base_url('assets/images/brunchwork-logo.png'); ?>" alt="">
	<div class="fifty-spacer"></div>
	<h1 class="h3 mb-3 font-weight-normal">Reset Password</h1>
	<label for="new_password" class="sr-only">New Password</label>
	<?php echo form_input($new_password);?>
	<label for="new_password" class="sr-only">New Password Confirm</label>
	<?php echo form_input($new_password_confirm);?>

	<?php echo form_input($user_id);?>
	<?php echo form_hidden($csrf); ?>

	<button class="btn btn-lg btn-brunchwork btn-block" type="submit">Submit</button>

<?php echo form_close();?>
</div>