<style type="text/css">.alert p:nth-child(2),.alert p:nth-child(3) {display: none;}</style>
<div class="box shadowed-box">
<?php $attributes = array('class' => 'form-signin text-center'); ?>
<?php echo form_open(base_url('forgot_password'), $attributes);?>
<img src="<?php echo base_url('assets/images/brunchwork-logo.png'); ?>" alt="">
<div class="fifty-spacer"></div>
<h1 class="h3 mb-3 font-weight-normal">Forgot your password?</h1>
<?php
      if($message != ''){
?>
<div class="alert alert-<?php echo $alert_type; ?> text-left" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
      </button>
      <?php 
      	//$message =  str_replace("Identity Already Used or Invalid","",$message);
      	//$message =  str_replace("Unable to Create Account","",$message);
      	//$message .= "<p>&nbsp;</p>If you are a new member, we will email you when you have Portal access. Stay tuned!";
      ?>
      <?php echo $message; ?>
</div>
<?php            
      }
?>
<p class="text-center" style="font-weight:700;">Enter your email below and we'll send you a link to reset your password.</p>
<label for="inputEmail" class="sr-only">Email address</label>
<input type="email" name="username" id="inputEmail" class="form-control forgotpass" placeholder="Enter Your Email" required autofocus>
<div class="twenty-spacer"></div>
<button class="btn btn-lg btn-brunchwork btn-block btn-signin" type="submit">SUBMIT</button>
<p class="mt-5 mb-3 text-muted"><!--&copy; 2018--></p>
<?php echo form_close();?>
</div>