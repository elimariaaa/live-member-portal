
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Membership extends Auth_Controller {
 
    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->model('UserModel');
        $this->load->library("braintree_lib");
    }
    
    public function index()
    {
        if($this->ion_auth->logged_in()){
            $this->data['pagetitle'] = 'brunchwork | Membership';
            //$this->render('member/membership');
            redirect('settings');
        } else {
            redirect('login');
        }
    }

    private function printJSON($var){
        echo json_encode($var);
    }

    public function get_token()
    {
        return $token = $this->braintree_lib->create_client_token();
        //$this->printJSON($token);
    }

    function get_clients(){
        $this->printJSON($this->braintree_lib->get_all_clients());
    }

    function get_client_data(){
        $client_data = $this->braintree_lib->client_fetch_id('1499265334');
        print_r($client_data);
        foreach($client_data as $customer) {
            echo $customer->firstName.'';
        }
    }

    function active_subscriptions(){
        $active_subscriptions = $this->braintree_lib->active_subscriptions();
        $counter = 5;
        foreach($active_subscriptions as $subscription) {
            if($counter == 0){
                die();
            }
            echo 'Subscription ID: '.$subscription->id.'<br />';
            echo 'merchantAccountId: '.$subscription->merchantAccountId.'<br />';
            echo 'planId: '.$subscription->planId.'<br /><br />';

            $counter--;
        }
    }

    function settings(){
        //declare variables first
        $first_bill_date = '';
        $new_date = '';
        $first_bill_year = '';
        $first_bill_month = '';
        $first_bill_day = '';
        $current_membership_date = '';
        $new_renewal_date = '';
        $address = '';
        $credit_card_transactions = '';
        $customer_braintree_id = '';
        $planId = '';
        $next_billing_date = '';
        $plan_name = '';
        $country_name = '';
        $this->data['token'] = '';
        if($this->ion_auth->logged_in()){
            $this->data['pagetitle'] = 'brunchwork | Settings';
            $user = $this->ion_auth->user()->row();
            //get plan
            $plans = $this->braintree_lib->all_plans();
            //1. get customer id using email address of logged in user
            $braintree_customer = $this->braintree_lib->find_client('email', $user->email);
            
            //print_r($braintree_customer); die();
            if($braintree_customer){
                foreach($braintree_customer AS $customer_details){
                    $customer_braintree_id = $customer_details->id;
                    $credit_card_transactions = $customer_details->creditCards;
                    foreach($credit_card_transactions AS $transactions){
                        $subscriptions = $transactions->subscriptions;
    
                        foreach($subscriptions AS $get_subs){
                            $planId = $get_subs->planId;
                            $next_billing_date = $get_subs->nextBillingDate;
                        }
                    }
                    //address
                    $address = $customer_details->addresses;
                }
            }
            
            if($address){
                $this->data['address_button'] = 'Update';
                foreach($address AS $get_address){
                    $this->data['address_id'] = ($get_address) ? $get_address->id:'';
                    $this->data['street_address'] = ($get_address) ? $get_address->streetAddress:'';
                    $this->data['extended_address'] = ($get_address) ? $get_address->extendedAddress:'';
                    $this->data['locality'] = ($get_address) ? $get_address->locality:'';
                    $this->data['region'] = ($get_address) ? $get_address->region:'';
                    $this->data['postalCode'] = ($get_address) ? $get_address->postalCode:'';
                    $this->data['countryCodeAlpha2'] = ($get_address) ? $get_address->countryCodeAlpha2:'';
                    $this->data['countryCodeAlpha3'] = ($get_address) ? $get_address->countryCodeAlpha3:'';
                    $this->data['countryCodeNumeric'] = ($get_address) ? $get_address->countryCodeNumeric:'';
                    $this->data['countryName'] = $country_name = ($get_address) ? $get_address->countryName:'';
                }

            } else {
                $this->data['address_button'] = 'Add';
                $this->data['address_id'] = '';
                $this->data['street_address'] = '';
                $this->data['extended_address'] = '';
                $this->data['locality'] = '';
                $this->data['region'] = '';
                $this->data['postalCode'] = '';
                $this->data['countryCodeAlpha2'] = '';
                $this->data['countryCodeAlpha3'] = '';
                $this->data['countryCodeNumeric'] = '';
                $this->data['countryName'] = '';
            }

            if($credit_card_transactions){
                foreach($credit_card_transactions AS $get_transactions){
                    if($get_transactions->default == 1){
                        $this->data['cc_bin'] = $get_transactions->bin;
                        $this->data['token'] = $get_transactions->token;
                    }
                }
            } else {
                $this->data['cc_bin'] = '';
            }

            $transactions = $this->braintree_lib->get_transactions($customer_braintree_id);
            print_r($transactions); die();
            //get plan name
            foreach($plans AS $get_plans){
                if($planId == $get_plans->id){
                    $plan_name = $get_plans->name;
                }
            }
            //renewal date
            $this->data['renewal_date'] = ($next_billing_date) ? $next_billing_date->format('F j, Y') : 'N/A'; 
            $this->data['subscriber_plan'] = $plan_name ? $plan_name : 'None';
            $this->data['email_address'] = $user->email;
            $this->data['states'] = $this->UserModel->get_states();
            $this->data['address'] = ($address) ? $address : '';
            $this->data['transactions'] = ($transactions) ? $transactions : '';

            //countries
            $this->data['countries'] = $this->UserModel->get_countries();
            $this->data['client_token'] = $this->get_token();
            $this->data['customer_braintree_id'] = $customer_braintree_id;
            $this->render('member/account/settings');
        } else {
            redirect('login');
        }
    }

    function cancel_membership(){
        $user = $this->ion_auth->user()->row();
        if($user->active == 1){// && strtolower($user->braintree_subscription_status) == 'canceled'
            $this->render('member/cancel_survey');
        } else {
            redirect('login');
        }
    }

    function merge(){
        $offset = $this->input->get('offset');
        $ch = curl_init();
        $api_key = '9ddf083e996a664899b80c0af6857e6a-us11';

        //list ID's from Mailchimp (FIXED)
        $dev_test_audience = '6064761ac2';
        $brunchwork_audience = '8bc8562f52';
        $test_audience = '14e8f02f9f';
        $deliverability_Test_2019_audience = '162b091d28';
        $nik_sharma_audience = '256f1ef295';
        $dummy_audience = '925803211f';
        $glockapps_delivery_monitor_seeds_audience = 'aceb02d2d0';
        $getahead_audience = 'd4771a8186';

        /*1. get emails from the Mailchimp list*/
        curl_setopt($ch, CURLOPT_URL, 'https://us11.api.mailchimp.com/3.0/lists/'.$brunchwork_audience.'/members?offset='.$offset.'&count=1000');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"name\":\"Freddie''s");
        curl_setopt($ch, CURLOPT_USERPWD, 'anystring' . ':' . $api_key);

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        $member_data = json_decode($result, true);
        echo "<pre>";
        
        $email_arr = array();

        foreach($member_data['members'] AS $member):
            $email_arr[] = $member['email_address'];
        endforeach;
        print_r($email_arr);
        /*2. get all the data in the database that is equal to the email list from MailChimp*/

        $connection = new PDO("mysql:host=localhost;dbname=brunworkdb", "brunworkdb", "5eF41R19#O9k", array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ));
        
        $arranged_email = "'".implode("','", $email_arr)."'";
        /*3. get referral count and referral URL */
        $sql = 'SELECT a.id, a.email_id, a.referal_url AS referral_url, COUNT(r.reference_user_id) AS referral_count FROM affliate_user a LEFT JOIN affliate_reffers r ON a.id = r.affliate_user_id WHERE a.email_id IN ('.$arranged_email.') GROUP BY a.id, a.email_id, a.referal_url';
        $stmt = $connection->prepare($sql); 
        $stmt->execute();
        $referrer_data = $stmt->fetchAll(PDO::FETCH_ASSOC);//(PDO::FETCH_ASSOC);
        $update_member_data = array();
        $existing_email_arr = array();
        $error_arr = array();
        if($stmt->rowCount() > 0){
            $ch2 = curl_init();
            $headers = array();
            $headers[] = 'Content-Type: application/json';
            foreach($referrer_data AS $referrer):
                $update_member_data = array(
                        'merge_fields' => array(
                            'REF_COUNT' => $referrer['referral_count'],
                            'REF_URL' => 'https://brunchwork.com/newsletter?ucode='.$referrer['referral_url']
                            )
                        );
                /*4. update referral count and referral URL in Mailchimp*/
                curl_setopt($ch2, CURLOPT_URL, 'https://us11.api.mailchimp.com/3.0/lists/'.$brunchwork_audience.'/members/'.md5(strtolower($referrer['email_id'])));
                curl_setopt($ch2, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch2, CURLOPT_POSTFIELDS, json_encode($update_member_data));
                curl_setopt($ch2, CURLOPT_USERPWD, 'anystring' . ':' . $api_key);
                curl_setopt($ch2, CURLOPT_HTTPHEADER, $headers);
                $existing_email_arr[] = $referrer['email_id'];
                $response = curl_exec($ch2);
                print_r(json_decode($response));
                if (!$response) 
                {
                    $error_arr[] = array('returned_false' => $referrer['email_id']);
                }
            endforeach;
            curl_close($ch2);
        }
    }
}