<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Ion Auth Model
 * @property Bcrypt $bcrypt The Bcrypt library
 * @property Ion_auth $ion_auth The Ion_auth library
 */
class UserModel extends CI_Model
{

    function get_min_rsvp($user_id = false){
        $this->db->select('min(rsvp_date) AS rsvp_date');
        $this->db->where('user_id', $user_id);
        $this->db->where('rsvp_date !=', NULL);
        $this->db->order_by('id');
        $this->db->limit(1);
        $result = $this->db->get('ci_rsvp');
        if ($result->num_rows()) {
            return $result->row();
        } else {
            return false;
        }
    }

    function get_cities(){
        return $this->db->where('active', 1)
                        ->get('ci_city_membership')->result_array();
    }

    function get_membership_plan(){
        $user = $this->ion_auth->user()->row();
        return $this->db->where('id', $user->ci_membership_plan_id)
                        ->get('ci_membership_plan')->row();
    }

    function get_city(){
        $user = $this->ion_auth->user()->row();
        return $this->db->where('id', $user->city_id)
                        ->get('ci_city_membership')->row();
    }

    function get_states(){
        $states = $this->db->get('ci_states')->result_array();
        $states_data = '<option value="" selected disabled>Select one.</option>';
        foreach($states AS $states_db_data){
            $states_data .= '<option value="'.$states_db_data['state_code'].'">'.$states_db_data['state_name'].'</option>';
        }
        return $states_data;
    }

    function get_industry(){
        return $this->db->select('industry')->where('industry !=', '')->group_by('industry')->order_by('industry', 'ASC')->get('ci_users')->result_array();
    }

    function submit_credit_card($cardnumber = false, $expiry_month = false, $expiry_year = false, $cvc = false){
        $user = $this->ion_auth->user()->row();
        $status = array();
        if($cardnumber && $expiry_month && $expiry_year && $cvc){
            if($this->check_card()){
                $update = $this->db->set('card_no', $cardnumber)
                                ->set('expiry_month', $expiry_month)
                                ->set('expiry_year', $expiry_year)
                                ->set('cvc', $cvc)
                                ->where('user_id', $user->id)
                                ->update('ci_credit_card');
                if($update){
                    return $status['status'] = 'update';
                } else {
                    return $status['status'] = 'false'; 
                }
            } else {
                $data = array(
                    'user_id' => $user->id,
                    'card_no' => $cardnumber,
                    'expiry_month' => $expiry_month,
                    'expiry_year' => $expiry_year,
                    'cvc' => $cvc
                );
            
                $insert = $this->db->insert('ci_credit_card', $data);
                if($insert){
                    return $status['status'] = 'insert';
                } else {
                    return $status['status'] = 'false'; 
                }
            }
        } else {
            return false;
        }
    }

    function get_countries(){
        return $this->db->get('ci_countries')->result_array();
    }

    function update_email(){
        $user = $this->ion_auth->user()->row();
        $data = array();
        $data['success'] = false;
        $data['message'] = '';
        //lindsey@aerointeractive.com
        //check first if email already exists
        $check_email_yours = $this->db->where('email', html_escape($this->input->post('update_email')))->where('id', $user->id)->get('ci_users')->num_rows();
        $check_email_not_yours = $this->db->where('email', html_escape($this->input->post('update_email')))->where('id !=', $user->id)->get('ci_users')->num_rows();
        if($check_email_yours > 0){
            $data['message'] = 'You entered the same email. Please try again.';
            return $data;
        }

        if($check_email_not_yours > 0){
            $data['message'] = 'The email you entered is not yours. Please try again.';
            return $data;
        }
        if(!$check_email_not_yours && !$check_email_yours){
            $this->db->set('email', html_escape($this->input->post('update_email')));
            $this->db->set('username', html_escape($this->input->post('update_email')));
            $this->db->where('id', $user->id);
            $update_email = $this->db->update('ci_users');
            $data['success'] = $update_email;
            return $data;
        }
    }

    function upload_image_path($target_file = false){
        $user = $this->ion_auth->user()->row();
        $this->db->set('profile_image', $target_file);
        $this->db->where('id', $user->id);
        return $this->db->update('ci_users');
    }

    function update_profile(){
        $user = $this->ion_auth->user()->row();
        $this->db->where('id', $user->id);
        return $this->db->update('ci_users', $this->input->post());
    }

    function get_members(){
        if($this->input->get()){
            $this->db->where('email LIKE "%'.$this->input->get('q').'%" OR first_name LIKE "%'.$this->input->get('q').'%" OR last_name LIKE "%'.$this->input->get('q').'%" OR CONCAT(first_name, " ", last_name) LIKE "%'.$this->input->get('q').'%"');
        }
        return $member_data = $this->db->select('CONCAT(first_name, \' \', last_name) AS text')->select('email as id')->where('active', 1)->order_by('last_name', 'ASC')->get('ci_users')->result_array();
    }

    function update_user($update_data_arr){
        $user = $this->ion_auth->user()->row();
        return $this->db->where('id', $user->id)->update('ci_users', $update_data_arr);
    }

    function update_userdata($update_data_arr, $subscription_id){
        return $this->db->where('braintree_subscription_id', $subscription_id)->update('ci_users', $update_data_arr);
    }

    function insert_survey(){
        $this->db->insert('ci_cancel_membership_survey', $this->input->post());
        $last_inserted_id = $this->db->insert_id();
        $user = $this->ion_auth->user()->row();
        $id_arr = array('user_id' => $user->id);
        return  $this->db->where('id', $last_inserted_id)->update('ci_cancel_membership_survey', $id_arr);
    }


    function check_pass_limit(){
        $user = $this->ion_auth->user()->row();
        $user_id = $user->id;
        $this->db->select('monthly_events');
        $this->db->where('id', $user_id);
        $pass_count = $this->db->get('ci_users')->result_array();
        return $pass_count;
    }

    function update_pass_limit(){
        /*$user = $this->ion_auth->user()->row();
        $user_id = $user->id;
        $this->db->select('monthly_events');
        $this->db->where('id', $user_id);
        $pass_count = $this->db->get('ci_users')->result_array();*/
        $monthly_events = $this->check_pass_limit();
        //print_r($monthly_events[0]);
        if($monthly_events[0]['monthly_events'] != "unlimited"){
            //echo "else";  print_r($monthly_events[0]); exit;
            $updated_event_limit = $monthly_events[0]['monthly_events'] - 1;
            $user = $this->ion_auth->user()->row();
            $limit_arr = array('monthly_events' => $updated_event_limit);
            return $this->db->where('id', $user->id)->update('ci_users', $limit_arr);
        }
        return true;
    }

    function rsvp(){
        //check if customer is already rsvp'd
        $user = $this->ion_auth->user()->row();
        $user_id = $user->id;
        $event_id = $this->input->post('event_id');
        $rsvp_arr = array(
            'event_id' => $event_id,
            'user_id' => $user_id
        );
        $this->db->where($rsvp_arr);
        $check_exist = $this->db->get('ci_rsvp')->row();
        if($check_exist != ''){
            return $check_exist->id;
        } else {
            $this->db->insert('ci_rsvp', $rsvp_arr);
            return $this->db->insert_id();
        }
    }

    function get_rsvp(){
        $user = $this->ion_auth->user()->row();
        $this->db->select('event_id');
        $this->db->where('user_id', $user->id);
        return $this->db->get('ci_rsvp')->result_array();
    }


    function get_guests($email_id, $event_id){
        $this->db
        ->select('ci_guest_pass.first_name as guest_first_name')
        ->select('ci_guest_pass.last_name as guest_last_name')
        ->select('ci_guest_pass.email as guest_email')
        ->select('ci_users.email as member_email')
        ->from('ci_guest_pass')
        //->join('ci_rsvp', 'ci_rsvp.event_id = ci_guest_pass.event_id')
        ->join('ci_users', 'ci_users.id = ci_guest_pass.user_id')
        ->where('ci_guest_pass.event_id', $event_id)
        ->where('ci_users.email', $email_id);
        //echo "<pre>"; print_r($this->db->get()->result_array()); echo "</pre>"; exit;
        return $this->db->get()->result_array(); 
    }

    function get_all_rsvp(){
        $this->db
            ->select('ci_users.first_name as member_first_name')
            ->select('ci_users.last_name as member_last_name')
            ->select('ci_users.email as member_email')
            
            ->select('ci_rsvp.user_id')
            ->select('ci_rsvp.event_id')
            ->from('ci_rsvp')
            ->where('ci_users.id !=', 280)
            ->join('ci_users', 'ci_users.id = ci_rsvp.user_id', 'left')
            ->group_by('ci_rsvp.user_id, ci_rsvp.event_id');
            return $this->db->get()->result_array();
        /*
	   $this->db
        ->select('ci_users.first_name as member_first_name')
        ->select('ci_users.last_name as member_last_name')
        ->select('ci_users.email as member_email')
        ->select('ci_guest_pass.id')
        ->select('ci_guest_pass.user_id')
    	//->select('ci_rsvp.user_id')
    	->select('ci_rsvp.event_id')
    	//->select('ci_rsvp.id')
        ->from('ci_rsvp')
        ->join('ci_guest_pass', 'ci_guest_pass.event_id = ci_rsvp.event_id', 'left')
        ->join('ci_users', 'ci_users.id = ci_guest_pass.user_id')
        ->group_by('ci_guest_pass.id');
        
        //  echo "<pre>"; print_r($this->db->get()->result_array()); echo "</pre>"; exit;
        return $this->db->get()->result_array(); 
        */
   }

   function member_has_guests($m_id, $e_id){
        $count  = $this->db->where('user_id', $m_id)->where('event_id', $e_id)->count_all_results('ci_guest_pass');
        //print_r($count); exit;
        if($count > 0){
            return true;
        }else{
            return false;
        }
   }


   function get_event_rsvp($event_id){
        $this->db
            ->select('ci_users.first_name as member_first_name')
            ->select('ci_users.last_name as member_last_name')
            ->select('ci_users.email as member_email')
            ->select('ci_users.id as member_id')
            ->select('ci_rsvp.user_id')
            ->select('ci_rsvp.event_id')
            ->select('ci_rsvp.rsvp_date')
            ->from('ci_rsvp')
            ->select('ci_users.company')
            ->select('ci_users.position')
            ->where('ci_users.id !=', 280)
            ->where('ci_rsvp.event_id', $event_id)
            ->join('ci_users', 'ci_users.id = ci_rsvp.user_id', 'left')
            ->group_by('ci_rsvp.user_id, ci_rsvp.event_id, ci_rsvp.rsvp_date');
            return $this->db->get()->result_array();
        /*
       $this->db
        ->select('ci_users.first_name as member_first_name')
        ->select('ci_users.last_name as member_last_name')
        ->select('ci_users.email as member_email')
        ->select('ci_guest_pass.id')
        ->select('ci_guest_pass.user_id')
        //->select('ci_rsvp.user_id')
        ->select('ci_rsvp.event_id')
        //->select('ci_rsvp.id')
        ->from('ci_rsvp')
        ->join('ci_guest_pass', 'ci_guest_pass.event_id = ci_rsvp.event_id', 'left')
        ->join('ci_users', 'ci_users.id = ci_guest_pass.user_id')
        ->group_by('ci_guest_pass.id');
        
        //  echo "<pre>"; print_r($this->db->get()->result_array()); echo "</pre>"; exit;
        return $this->db->get()->result_array(); 
        */
   }



   function get_guests_download($event_id){
       $this->db
        ->select('ci_users.email as member_email')
        ->select('ci_rsvp.id')
        ->from('ci_rsvp')
        ->join('ci_users', 'ci_users.id = ci_rsvp.user_id')
        ->where('ci_rsvp.event_id', $event_id);
        //->group_by('ci_rsvp.event_id');
        
        //echo "<pre>"; print_r($this->db->get()->result_array()); echo "</pre>"; exit;
        return $this->db->get()->result_array(); 
   }

   function get_all_guests($event_id = false){
       return $this->db->where('event_id', $event_id)->count_all_results('ci_guest_pass');
   }

   function get_all_guests_data($event_id = false){
    return $this->db
                    ->select('g.*, u.first_name AS member_first_name, u.last_name AS member_last_name, u.email AS member_email')
                    ->from('ci_guest_pass g')
                    ->where('g.event_id', $event_id)
                    ->join('ci_users u', 'g.user_id = u.id', 'left')
                    ->get()
                    ->result_array();
    }
    function cancel_rsvp(){
        $user = $this->ion_auth->user()->row();
        $event_id = $this->input->post('event_id');
        $this->db->where('event_id', $event_id);
        $this->db->where('user_id', $user->id);
        $this->db->delete('ci_rsvp');
        $monthly_events = $this->check_pass_limit();
        if($monthly_events[0]['monthly_events'] != 'unlimited'){
            $updated_event_limit = $monthly_events[0]['monthly_events'] + 1;
            $limit_arr = array('monthly_events' => $updated_event_limit);
            return $this->db->where('id', $user->id)->update('ci_users', $limit_arr);
        }
        return true;
    }

    function count_rsvp($event_id = false){
        $this->db->where('event_id', $event_id);
        return $this->db->count_all_results('ci_rsvp');
    }

    function get_email_rsvp($rsvp_id = false){
        $this->db->where('id', $rsvp_id);
        return $this->db->get('ci_rsvp')->row();
    }

    function get_user_data($user_id = false){
        $this->db->where('id', $user_id);
        return $this->db->get('ci_users')->row();
    }

    function get_all_members_with_subscription(){
        $this->db->where('braintree_subscription_id !=', '');
        $this->db->where('braintree_plan_id', '');
        return $this->db->get('ci_users')->result_array();
    }

    function check_braintree_status($username = false){
        return $this->db->where('username', $username)
                        ->where('email', $username)
                        ->get('ci_users')
                        ->row();
    }

    function get_users_with_missing_data(){
        return $this->db->where("`id` != 1 AND (`braintree_plan_id` = '' OR `first_bill_date` IS NULL OR `braintree_subscription_status` IS NULL) AND braintree_subscription_id IS NOT NULL")
                        ->get('ci_users')->result_array();
    }

    function update_users_with_missing_data($braintree_subscription_id = false, $update_data_arr){
        $this->db->where('braintree_subscription_id', $braintree_subscription_id)->update('ci_users', $update_data_arr);
    }
}
