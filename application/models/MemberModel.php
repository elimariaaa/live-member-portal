<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Ion Auth Model
 * @property Bcrypt $bcrypt The Bcrypt library
 * @property Ion_auth $ion_auth The Ion_auth library
 */

class MemberModel extends CI_Model
{
    public function __construct() {
        parent::__construct(); 
    }

    function get_members($rowno,$rowperpage){
        
        $user = $this->ion_auth->user()->row();
		$active_mem = 1;
        if($this->input->get()){
            if($this->input->get('member_data') && $this->input->get('member_data') != ''){
                $member = $this->input->get('member_data');
                $where = "(CONCAT(`first_name`, ' ', `last_name`) LIKE '%".$member."%' OR `first_name` LIKE '%".$member."%' OR `last_name` LIKE '%".$member."%')";
                $this->db->where($where);
            }
            if($this->input->get('member_industry') && $this->input->get('member_industry') != ''){
                $industry = $this->input->get('member_industry');
                if($industry == "All"){
                    
                }else{
                    $this->db->like('u.industry', $industry);
                }
            }
            if($this->input->get('member_city') && $this->input->get('member_city') != ''){
                $member_city = $this->input->get('member_city');
                if($member_city == "All"){
                    
                }else{
                    $this->db->where('u.city_id', $member_city);
                }
            }
			if($this->input->get('member_alumni') && $this->input->get('member_alumni') != ''){
				$member_alumni = $this->input->get('member_alumni');
				if($member_alumni == "active"){
					$active_mem = 1;
				}else{
					$active_mem = 0;
					$this->db->where('u.alumni', "Inactive");
				}
			}
        }
        /*echo $rowperpage;
        echo $rowno;
        exit;*/
        $this->db->from('ci_users u');
        $this->db->join('ci_users_groups g', 'u.id = g.user_id', 'left');
        /* users can search themselves */
        //$this->db->where('u.id !=', $user->id);
        // hiding admin from search
        $this->db->where('u.id !=', 279);
        $this->db->where('u.id !=', 280);
        $this->db->where('g.group_id !=', 1);
        $this->db->where('u.active', 1);
		if($active_mem == 1){
			$this->db->where('u.alumni', "Active");
		}
        $this->db->limit($rowperpage, $rowno);
        $this->db->order_by('first_name', 'asc');
        $this->db->order_by('last_name', 'asc');
        return $this->db->get()->result_array();
    }


    // Select total records
    public function getrecordCount() {
        $user = $this->ion_auth->user()->row();
        if($this->input->get()){
            if($this->input->get('member_data') && $this->input->get('member_data') != ''){
                $member = $this->input->get('member_data');
                $where = "(CONCAT(`first_name`, ' ', `last_name`) LIKE '%".$member."%' OR first_name LIKE '%".$member."%' OR last_name LIKE '%".$member."%')";
                $this->db->where($where);
            }
            if($this->input->get('member_industry') && $this->input->get('member_industry') != ''){
                $industry = $this->input->get('member_industry');
                if($industry == "All"){
                    
                }else{
                    $this->db->like('u.industry', $industry);
                }
            }
            if($this->input->get('member_city') && $this->input->get('member_city') != ''){
                $member_city = $this->input->get('member_city');
                if($member_city == "All"){
                    
                }else{
                    $this->db->where('u.city_id', $member_city);
                }
            }
        }
        $this->db->select('count(*) as allcount');
        $this->db->from('ci_users u');
        $this->db->join('ci_users_groups g', 'u.id = g.user_id', 'left');
        $this->db->where('u.id !=', $user->id);
        $this->db->where('g.group_id !=', 1);
        $this->db->where('u.active', 1);
        $query = $this->db->get();
        $result = $query->result_array();

        return $result[0]['allcount'];
    }

    function get_all_city(){
        return $this->db->where('active', 1)->order_by('id')->get('ci_city_membership')->result_array();
    }

    function get_all_tags(){
        return $this->db->select('industry')->where('industry !=', " ")->group_by('industry')->get('ci_users')->result_array();
    }

    function get_braintree_plans(){
        return $this->db->get('ci_braintree_plan_new')->result_array();
    }
}
